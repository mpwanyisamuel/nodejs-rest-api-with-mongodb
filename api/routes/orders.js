const express = require('express');
const router  = express.Router();

// orders/
router.get('/', (req, res, next) => {
    res.status(200).json({ message: 'GET request for all orders' });
});

router.post('/', (req, res, next) => {
    res.status(201).json({ 
        message: 'Post request for creating Order' 
    });
});

router.get('/:orderId', (req, res, next) => {
    res.status(200).json({ 
        message: 'Order details',
        order: req.params.orderId 
    });
});

// Delete an item
router.delete('/:orderId', (req, res, next) => {
    res.status(200).json({ 
        message: 'Order deleted',
        order: req.params.orderId 
    });
});

module.exports = router;