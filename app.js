const express = require('express');
const app = express();
const morgan = require('morgan');
const bodyParser = require('body-parser');

const mongoose = require('mongoose');
import con from './connection';

const connectioString = `mongodb+srv://${con.username}:${con.password}@restapiwithmongo.spews.mongodb.net/${con.dbName}?retryWrites=true&w=majority`;

mongoose.connect(
    connectioString,
    { useNewUrlParser: true, useUnifiedTopology: true }
).then(()=> {
    console.log('Connected to mongo DB Atlas!')
}).catch(err => {
    console.log('connection to mongo failed!')
})

const productsRoute = require('./api/routes/products');
const ordersRoutes = require('./api/routes/orders');

// calls next inside routes
app.use(morgan('dev'));

// used to read request body
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Handle CORS
const allowedHosts = '*'; // Allow all
// Specifically -> 'http://localhost:8080

app.use((req, res, next) => {
    res.header('Access-Control-Allow-Origin', allowedHosts);
    res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization'
    );
    if (req.method === 'OPTIONS') { // What methods to allow
        req.header(
            'Access-Control-Allowed-Methods',
            'POST, PUT, PATCH, GET, DELETE'
        );
        return res.status(200).json({});
    }
    next();
});

// my routes
app.use('/products', productsRoute);
app.use('/orders', ordersRoutes);

// Error handler
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    })
});

module.exports = app;